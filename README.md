# GP2D

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Usage

The image will attempt to run your Makefile. Simply run:

```bash
$ docker run -v ${PWD}:/data registry.gitlab.com/yorkcs/batman/gp2d
```

A `gp2d` command will be available within this image, which can be used to compile GP2 style graphs to dot format. It the input filename as a single argument.

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/gp2d:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/gp2d
```

## Development

The production container image building does not reuse partial compilation results, nor does it show the Haskell files produced by BNFC. We detail how to work around this in development.

Building the parser can be performed by:

```bash
$ cd lib/parser && docker run -v ${PWD}:/data registry.gitlab.com/yorkcs/batman/bnfc-haskell rule && cd ../../
```

Building the project is then:

```bash
$ docker run -i -v ${PWD}:/data haskell:8.6 ghc -i:./data/src -i.:/data/lib/src -i.:/data/lib/parser /data/src/Main.hs
```

Running the project on `test.gp2`:

```bash
$ docker run -i -v ${PWD}:/data haskell:8.6 /data/src/Main /data/test.gp2
```
