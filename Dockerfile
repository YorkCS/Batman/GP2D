FROM registry.gitlab.com/yorkcs/batman/bnfc-haskell:latest
COPY ./lib/parser /parser
WORKDIR /parser
RUN run.sh rule

FROM haskell:8.10-buster
COPY ./ /data
RUN rm -rf /data/lib/parser
COPY --from=0 /parser /data/lib/parser
RUN ghc -i:./data/src -i:./data/lib/src -i:./data/lib/parser /data/src/Main.hs

FROM buildpack-deps:buster
COPY --from=1 /data/src/Main /usr/local/bin/gp2d
RUN mkdir /data
WORKDIR /data
ENTRYPOINT ["gp2d"]
