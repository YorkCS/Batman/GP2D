module Main where

import Control.Monad
import System.Environment (getArgs)
import System.Exit (exitFailure, exitSuccess, exitWith, ExitCode(ExitFailure))

import ErrM

import LexRule (Token)
import qualified AbsRule
import ParRule (myLexer, pGraph)

import Common
import Printer (renderGraph)
import Processor (convertGraph)


type Lexer = String -> [Token]
type Parser a = [Token] -> Err a
type Processor a b = a -> Err b
type Pipeline a = String -> Err a


pipeline :: Lexer -> Parser (AbsRule.Graph a) -> Processor (AbsRule.Graph a) ([NodeKey], Graph) -> String -> Err Graph
pipeline lex par pro str =
  let
    t = lex str
  in case par t of
    Bad err ->
      fail $ unlines ["Parse failed: " ++ err, "Tokens: " ++ show t]
    Ok g ->
      case pro g of
        Ok (_, g) -> return g
        Bad err -> fail $ "Analysis failed: " ++ err


run :: Pipeline Graph -> String -> IO ()
run pln str =
  case pln str of
    Bad r -> do
      putStrLn r
      exitWith (ExitFailure 2)
    Ok g -> do
      putStrLn $ renderGraph True 0 g
      exitSuccess


main :: IO ()
main = do
  args <- getArgs
  case args of
    [f] -> readFile f >>= run (pipeline myLexer pGraph (convertGraph []))
    _ -> do
      putStrLn "Expected to be provided with a single input filepath"
      exitFailure
